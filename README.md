# 展板项目

## 项目介绍

展板项目是 JECloud 前端核心子项目之一，通过【开发-核心引擎-微应用管理】植入到 JECloud 中，后利用【顶部菜单】挂接到顶部菜单使用。包含首页展板、工作展板、管理展板、开发展板。

## 项目目录

```bash

│  .commitlintrc.js        # Git 提交校验配置文件
│  .editorconfig           # 编辑器配置文件
│  .env                    # 公共环境配置文件
│  .env.development        # 开发环境配置文件
│  .env.production         # 生产环境配置文件
│  .eslintignore           # eslint 忽略校验配置文件
│  .eslintrc.js            # eslint 校验配置文件
│  .gitattributes          # Git 配置文件，设置行末字符为LF
│  .gitignore              # Git 忽略提交配置文件
│  .prettierrc.js          # 代码格式化配置文件
│  babel.config.js         # babel配置文件
│  CHANGELOG.md            # Git 提交记录
│  package.json            # 项目配置
│  README.md               # 说明文档
│  LICENSE                 # 开源协议文件
│  SUPPLEMENTAL_LICENSE.md # 补充协议文件
├─.vscode                  # vscode 项目配置目录，不建议私自修改
│      extensions.json     # vscode 推荐插件
│      settings.json       # vscode 常用配置
├─build                    # 项目构建目录
│     ├─hooks-git          # git 钩子函数
│     └─webapck            # webpack配置
├─docs                     # 帮助文档
├─service                  # 系统文件，如果问题，可以反馈，不允许私自修改
├─public                   # 静态资源
└─src                      # 源码文件

```

## 开发环境

### node

`v 14.18.3`

### npm

`v 6.14.15`

## 基础库项目部署
本地项目调试之前，需要先把 [基础库项目](https://gitee.com/ketr/jecloud-pc-libs.git) 部署完成。
```bash
# 全局安装 yalc，lerna
npm i yalc lerna@^6.0.0 -g

http://verdaccio.jecloud.net/

## 项目命令

### 全局安装 [yalc](./docs/Yalc%20使用说明.md)

```bash
npm install yalc -g
```

### 安装依赖

```bash
# 非源码用户，不需要部署基础库项目，直接安装npm包依赖
npm run setup:lib

# 源码用户，开发调试用户，请部署本地基础库项目后，再安装依赖
npm run setup
```

### 启动服务

```bash
npm run dev
```

### 代码构建

```bash
npm run build
```

### Git 代码提交

项目增加了 Git 提交规范，强烈建议使用 `commitizen`（格式化 commit message 的工具）来进行 Git 提交操作，请使用下面命令

```bash
npm run commit
```

### 生成 Git 提交记录

```bash
# 先开启合并策略，防止骨架项目与插件项目的CHANGELOG.md文件冲突
# git config --global merge.ours.driver true
npm run changelog
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud 主目录

[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)
