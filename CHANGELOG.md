## [2.2.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.2.1...v2.2.2) (2023-12-29)


### Features

* **home:** 首页展板流程消息模板添加催办标识[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([a6ffebf](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/a6ffebf4e45af65b666b5c2fe4554834217244da))



## [2.2.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.2.0...v2.2.1) (2023-12-15)


### Bug Fixes

* **public:** 优化静态资源文件[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([9d1cedd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/9d1cedd12b451b0e12a4858aec932b3dfde0eea9))


### Features

* **boards:** 首页插件加流程紧急状态[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([8f8b4a1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/8f8b4a15ad0f0563461275fd3b9b70ecd02fbe32))
* **flie:** 添加图片预览资源 ([9cb8d34](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/9cb8d3471501c37c9182aeaca06bda2ea021f5db))
* **home:** 首页流程面添加紧急样式[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([f1f643f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f1f643f6c00b54e02d71fb7d781b430cacb78017))
* **version:** release v2.1.0 ([f5ca7b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f5ca7b9f44666d6509850aae42dbae53e9a9567f))
* **version:** release v2.1.1 ([6dc31ab](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/6dc31abb52c2ee4d69ba9e6b6d8bc1783de0b4a7))
* **version:** release v2.2.0 ([8d1a7a8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/8d1a7a8a8a4a3ccff04f887a890cb4868483680c))



# [2.2.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.1.1...v2.2.0) (2023-11-15)



## [2.1.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.1.0...v2.1.1) (2023-10-27)



# [2.1.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.0.9...v2.1.0) (2023-10-20)


### Bug Fixes

* **http:** 优化http请求的拦截器和退出登录的逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([61178f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/61178f33af0d27bbd0a7f1798e6e6eba8209810c))


### Features

* **version:** release v2.0.8 ([8970821](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/89708211d43becbdbff332f64005cf60c108b366))
* **version:** release v2.0.9 ([0fbfd4a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/0fbfd4add77a87758df961c9e95ef2e3687a307f))



## [2.0.9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.0.8...v2.0.9) (2023-09-22)



## [2.0.8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.0.7...v2.0.8) (2023-09-15)


### Bug Fixes

* **env:** 更新服务代理地址[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([25bafcb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/25bafcb0e894b9a126d1b7067e8ab7edf2e81cc5))


### Features

* **doc:** 文件changelog修改提交 ([0028b1a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/0028b1a8b43fc25201838cb86d94951dba5fe1ac))
* **script:** 增加setup:lib命令，提供非源码客户使用[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([a2911c9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/a2911c9b761941d3af6f7e73b5e7fb91f8aff203))
* **version:** release v2.0.4 ([abfadad](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/abfadad80aac3a9f264dff1c2c861d9dbcc0ffc8))
* **version:** release v2.0.5 ([14f06f4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/14f06f42d697b2fcd4132d72e9348059593347d5))
* **version:** release v2.0.6 ([75f479d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/75f479d97a391a8e43078bbae15e0d82c21b69a5))
* **version:** release v2.0.6 ([0e84c35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/0e84c358a52e1ca7df1040f58dfad18348faaa56))
* **version:** release v2.0.7 ([ca2a996](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/ca2a9961629a05cd7e9f4e8978e3446a558ce3e3))



## [2.0.7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.0.6...v2.0.7) (2023-09-08)



## [2.0.6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.0.5...v2.0.6) (2023-09-01)



## [2.0.5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.0.4...v2.0.5) (2023-08-25)



## [2.0.4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v2.0.3...v2.0.4) (2023-08-18)



## [2.0.3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v1.4.0...v2.0.3) (2023-08-12)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 修复websocket代理地址失效问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([6975273](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/69752730079a6686f1be71e01f454f4f24f07609))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **boards:** 共享给我头像加载不出来[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([8314310](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/83143109becc199cbc33e3b0372f7b5e4d40e541))
* **boards:** 开发展板快捷入口流程监控点击无反应[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([267aa72](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/267aa721b6576889382867f3efb0e443cba5ac68))
* **boards:** 首页样式变量修改 ([f80b9d9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f80b9d99efcacd394d04a76ea6e6661fa8627c16))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **home:** 流程和通知面板的数值联动变化[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([e04bac7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/e04bac71a86636fb985c512fb2d2960aae86e07a))
* **home:** 流程和通知面板的数值联动变化[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([174ff88](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/174ff88657af2da5b38f7116b1d27cb8125ed555))
* **home:** 首页展板修改通知弹出功能,并且流程和通知联动刷新通知[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([9bf90a2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/9bf90a2b14ad3f02c35862fc05e534dfa14323e3))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([1870915](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/18709152956b0409657deaa33ed9443d7654b464))
* **version:** release v2.0.0 ([073332f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/073332f9f1263200a53476c597c832e6f520a617))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([fdc7482](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/fdc74825a27f5795c88f43b101d0b00a141a8f23))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([c7299b8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c7299b86f6a8e61157723a86e5a9c09596fedf53))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))
* **version:** release v2.0.3 ([1793e6b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/1793e6b39724654d98563036fade36e4282d8b14))



## [2.0.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v1.4.0...v2.0.2) (2023-08-04)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **boards:** 共享给我头像加载不出来[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([8314310](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/83143109becc199cbc33e3b0372f7b5e4d40e541))
* **boards:** 开发展板快捷入口流程监控点击无反应[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([267aa72](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/267aa721b6576889382867f3efb0e443cba5ac68))
* **boards:** 首页样式变量修改 ([f80b9d9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f80b9d99efcacd394d04a76ea6e6661fa8627c16))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **home:** 流程和通知面板的数值联动变化[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([e04bac7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/e04bac71a86636fb985c512fb2d2960aae86e07a))
* **home:** 流程和通知面板的数值联动变化[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([174ff88](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/174ff88657af2da5b38f7116b1d27cb8125ed555))
* **home:** 首页展板修改通知弹出功能,并且流程和通知联动刷新通知[[#4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/4)] ([9bf90a2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/9bf90a2b14ad3f02c35862fc05e534dfa14323e3))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([1870915](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/18709152956b0409657deaa33ed9443d7654b464))
* **version:** release v2.0.0 ([073332f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/073332f9f1263200a53476c597c832e6f520a617))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([fdc7482](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/fdc74825a27f5795c88f43b101d0b00a141a8f23))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))



## [2.0.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v1.4.0...v2.0.1) (2023-07-30)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **boards:** 共享给我头像加载不出来[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([8314310](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/83143109becc199cbc33e3b0372f7b5e4d40e541))
* **boards:** 开发展板快捷入口流程监控点击无反应[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([267aa72](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/267aa721b6576889382867f3efb0e443cba5ac68))
* **boards:** 首页样式变量修改 ([f80b9d9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f80b9d99efcacd394d04a76ea6e6661fa8627c16))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([1870915](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/18709152956b0409657deaa33ed9443d7654b464))
* **version:** release v2.0.0 ([073332f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/073332f9f1263200a53476c597c832e6f520a617))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/d9b316b2671fb760e1456907c216adff855d4537))



# [2.0.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/compare/v1.4.0...v2.0.0) (2023-07-22)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **boards:** 共享给我头像加载不出来[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([8314310](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/83143109becc199cbc33e3b0372f7b5e4d40e541))
* **boards:** 开发展板快捷入口流程监控点击无反应[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/3)] ([267aa72](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/267aa721b6576889382867f3efb0e443cba5ac68))
* **boards:** 首页样式变量修改 ([f80b9d9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f80b9d99efcacd394d04a76ea6e6661fa8627c16))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([1870915](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/18709152956b0409657deaa33ed9443d7654b464))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-boards/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/compare/v1.4.0...v1.4.1) (2023-06-30)


### Bug Fixes

* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))


### Features

* **boards:** 共享给我头像加载不出来[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([8314310](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/83143109becc199cbc33e3b0372f7b5e4d40e541))
* **boards:** 开发展板快捷入口流程监控点击无反应[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([267aa72](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/267aa721b6576889382867f3efb0e443cba5ac68))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))



# [1.4.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/compare/v1.2.0...v1.4.0) (2023-06-06)


### Bug Fixes

* **ajax:** 修复主子应用共用ajax实例[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/issues/8)] ([94f40c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/94f40c21f97664e3f0150830f1e6e93ed1849aa9))
* **plugin:** 废弃@jecloud/plugin,调整JE的注册[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/issues/8)] ([052a918](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/052a918eec2e9cdab556e52fbecc727384ebecd4))
* **utils:** @jecloud/utils删除vue依赖[[#67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/issues/67)] ([e6929e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/e6929e0d0df43b58c15f3bdd41d20d525f30fa20))
* **utils:** 适配jecloud/utils的调整 ([47b8cdd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/47b8cdd79cfd1d2a459f809a5ebbf8bcd11ce8e5))


### Features

* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 图片预览区分节点加载[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/issues/11)] ([2876a2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/2876a2eed3a5e13047094d4034aff39e88bebfcf))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/affc489c088b3b77dbd2d176a887562726735540))
* **je:** 暴露JE常用方法 ([4efb75b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/4efb75b4881eae4000153638e1d0d4b4628547cb))
* **je:** 将vue的h函数添加到JE上 ([753fad9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/753fad92b4fbf4a7aabbcc29fdd3b950ad3f426a))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.3.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-pc-archetype/compare/v1.2.0...v1.3.0) (2023-05-05)


### Bug Fixes

* **ajax:** 修复主子应用共用ajax实例[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([94f40c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/94f40c21f97664e3f0150830f1e6e93ed1849aa9))
* **plugin:** 废弃@jecloud/plugin,调整JE的注册[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([052a918](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/052a918eec2e9cdab556e52fbecc727384ebecd4))
* **utils:** @jecloud/utils删除vue依赖[[#67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/67)] ([e6929e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e6929e0d0df43b58c15f3bdd41d20d525f30fa20))
* **utils:** 适配jecloud/utils的调整 ([47b8cdd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/47b8cdd79cfd1d2a459f809a5ebbf8bcd11ce8e5))


### Features

* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 图片预览区分节点加载[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/11)] ([2876a2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2876a2eed3a5e13047094d4034aff39e88bebfcf))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **bi:** 图标增加阴影并更改版本号[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([946be0c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/946be0c0bfea9042951a3c56add24dea9699d718))
* **boards:** 更新脚手架项目及libs[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([648d632](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/648d63246c6380656037b5662d1632bff20d52d1))
* **boards:** 更新package-lock文件及通知消息弹窗v-html显示内容[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([b37bc6a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/b37bc6aea194cc301a557f7535e1223135b1c2d8))
* **boards:** 首页展板搜索区域文字换行[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([3339b75](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/3339b75b705223ed6bc3707cfe306e20a4203d6a))
* **boards:** 首页展板搜索区域文字换行调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([01c0133](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/01c013378dcd5c2fb6cd4f0ed8cd413bf2c071f2))
* **boards:** 展板更新package-lock文件[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([6b19732](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6b197326faf0cc75a4b5d7ab8e0408ff30de02ea))
* **boards:** 展板快速入口图标增加背景阴影效果[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([ad97357](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ad97357b2ed2b4fe638a9b1501b822c488f080ce))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/affc489c088b3b77dbd2d176a887562726735540))
* **je:** 暴露JE常用方法 ([4efb75b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4efb75b4881eae4000153638e1d0d4b4628547cb))
* **je:** 将vue的h函数添加到JE上 ([753fad9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/753fad92b4fbf4a7aabbcc29fdd3b950ad3f426a))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/379dfe68892583c042d0e92a33d5db745f9fa629))
* **vresion:** release v1.3.0 ([14efa09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/14efa09a370f207775ac54d58df8ed0186c1a569))



# [1.3.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/compare/v1.2.0...v1.3.0) (2023-05-05)


### Features

* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/affc489c088b3b77dbd2d176a887562726735540))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.2.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/compare/v1.0.2...v1.2.0) (2023-04-06)


### Bug Fixes

* **build:** 修复样式文件采用参数引用方式 ([dc6fd3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/dc6fd3fecc0c02b536590915888f967333cdce44))
* **font:** 修复字体文件为版本参数引用 ([a353a02](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a353a02d0cf04447a92ae635604b4545a6a18f63))
* **i18n:** 修改欢迎页i18n文字，变量导致火狐52卡死[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([ae5b45c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ae5b45c45bc02fb81f0e80df6ad1b84686f0e7c4))


### Features

* **cli:** changelog提交 ([0a4c001](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0a4c001c14276bacd26ff9dd3b5798fab203a459))
* **cli:** packagelock文件提交 ([9282afd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/9282afd0bc17b871511e3d59bf7f9f2fee378440))
* **cli:** v1.1.0定版 ([11af62b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/11af62bd47230ee6d422effa56e4dd446b0ecca0))
* **init:** 增加初始化加载系统配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([a2820a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a2820a46173411a049263d9358473f79f6d6e64d))
* **md:** 火狐52浏览器适配文档提交 ([6b5c00b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6b5c00b76bcff13164ce0f79364c1f06f7afc882))
* **md:** 火狐52浏览器适配文档提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/10)] ([75c0262](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/75c0262173c62362ad83ef5be7dfef578ae159de))
* **pdf:** pdf预览资源 ([530c87e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/530c87e6eea990882732446b3ca3a8f74310968a))
* **version:** release v1.2.0 ([d0add8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/d0add8b04191d33377beb32b36df2c4cb29b87af))



# [1.1.0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/compare/v1.0.2...v1.1.0) (2023-03-03)


### Bug Fixes

* **code:** 调整代码，适应所有分支结构 ([2e7d448](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2e7d44879680fda6cf2af72b7c6b1fd4cc18ed5c))
* **i18n:** 修改欢迎页i18n文字，变量导致火狐52卡死[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([ae5b45c](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ae5b45c45bc02fb81f0e80df6ad1b84686f0e7c4))
* **icon:** 升级图标配置 ([ae975c4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ae975c4dec20dcb889971157953f7227826c56ab))
* **logout:** 修复退出登录接口[[#9](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/9)] ([e244604](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e244604c6aa1bfd01268e4cbf35af8e7a82afc44))
* **logout:** 增加退出登录接口调用[[#9](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/9)] ([bda04a2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/bda04a21469159c1973ab121b15c49c81f838425))
* **npm:** 废弃pnpm安装，采用原始npm安装依赖[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([9e653ec](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/9e653ec168b9b0ce44aafb2f59c656ba1f2811cd))
* **npm:** 更新npm lock文件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([11cbdc5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/11cbdc56dafbc7f0ada60ac8b0bf8716840ef493))
* **npm:** 修改npm私服地址 ([cb61aad](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/cb61aadb4077424573b8b7e8770972d0091aa569))


### Features

* **定版:** 1.0.1定版 ([8123224](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/812322456015b02cb45b5030a4492b15b1b2151a))
* **定版:** 1.01定版 ([8f03fe1](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/8f03fe15e8dd6d34ef029848a86bb0d7aa3d0931))
* **cli:** package-lock文件提交[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([9f37092](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/9f370921f4dd15533377b8ec432d149bc5500ef8))
* **home:** 国际化修改 ([95b4df4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/95b4df458b572e4b50d2dfa6f5ac0a1d382b3f81))
* **init:** 增加初始化加载系统配置[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([a2820a4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a2820a46173411a049263d9358473f79f6d6e64d))
* **locales:** 国际化方法修改[[#10](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/10)] ([ef3aef4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ef3aef469c19a2a90ae28339a6ecd48175e80557))
* **md:** 火狐52浏览器适配文档提交 ([6b5c00b](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6b5c00b76bcff13164ce0f79364c1f06f7afc882))
* **md:** 火狐52浏览器适配文档提交[[#10](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/10)] ([75c0262](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/75c0262173c62362ad83ef5be7dfef578ae159de))



# [1.0.0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/compare/28659bb098e260eee3fec6522782483d5defdf0b...v1.0.0) (2022-08-30)


### Bug Fixes

* **admin:** 主子应用打通 ([7afb056](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/7afb0562c91be650cd3fb38fb79e15868bc2506c))
* **ant:** 更新ant-design-vue@~3.1.0-rc ([4ca2bd3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4ca2bd307e68b797e96d8a9269cdde02ba795435))
* **antd:** 调整vite依赖引用 ([74942d5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/74942d5abe0b9579966d859f542fc6bb9c264d81))
* **antd:** 暂注掉antd图标按需加载，后期调试 ([c5b4766](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c5b476687c631a14dee0540080843dd7a90092ec))
* **assets:** 支持动态加载微应用资源 ([a121b57](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a121b57a0fbf949d3f4efc44d989dcc3a1e95449))
* **axios:** 修复axios格式化数据 ([145baff](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/145baff06da0a57e5b765b6ec85c21246923afae))
* **axios:** 修复axios格式化数据 ([1c98fbf](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/1c98fbf1e59027e16ef328dee9746e6b1f19eb82))
* **axios:** 增加axios微应用代理前缀prexyPrefix[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([76b6650](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/76b6650d43b09fbcb130bb4941729dec562497b0))
* **babel:** 修改按需加载引用目录为es ([38ac102](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/38ac102a89be1dc747856862090935197065fdb9))
* **base:** 根据开发规范修改文件命名[[#1](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/1)] ([d03badf](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/d03badf4aea253280b40eaf16e7fcd88208a3299))
* **bug:** 修复主题，登录等bug ([9116e9b](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/9116e9bb7524f8fe1b2f25870694c2f930baa002))
* **build:** 调整项目打包配置[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([8a20fe0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/8a20fe0fb4a0a3022dce708c5fe28ed9ebefbd5a))
* **build:** 去除sourceMap[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([0f91481](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0f914813b0e422cd8a26fafb8c9cf006a9483907))
* **build:** 剔除monaco打包处理[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([01b92ac](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/01b92acff392264ed061de55542104569796cba7))
* **build:** 提供公共资源的json配置文件，方便应用调用[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([3122798](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/3122798d67192219d4cf2cc44b25c6f98d2845bd))
* **build:** 修复微应用打包后的数据文件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([bc40d94](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/bc40d94ba4547e7e047933cf44e9ecc042aac8a9))
* **build:** 修复env配置文件 ([0044821](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0044821ee5f3107e1998dbac970fdf46524a727b))
* **build:** 修改webpack打包配置[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([395061c](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/395061c23f9612d753f930006957fa7c04e2ba98))
* **build:** 优化打包流程 ([3cf58a3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/3cf58a361e80f2fb3f10d39c157bb26a9c6ec7e0))
* **build:** 增加图片资源打包[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([5ef32f6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/5ef32f6b8deadbc6a526daac23892e06366c82c7))
* **build:** 主子应用打包使用相同配置[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([63ed9d3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/63ed9d34fd536af080e46e41c8b8b37e10af05ee))
* **code:** 调整代码结构 ([3ef4c2d](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/3ef4c2d69dde2428574985b7c5b3c68fa312824e))
* **css:** 统一调整antd个性化样式引入方式 ([d7d1bbd](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/d7d1bbdc3166a0de1982d75029428adae67de525))
* **doc:** 由于pnpm7不兼容yalc，不支持file:xxx安装本地包，所以请使用 pnpm6[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([c461e36](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c461e36575353d9814b02e5360f08e1e562a3b25))
* **envs:** 修改系统变量 ([015b6a2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/015b6a21b59a90e0cd7f89c531edf8085b231d06))
* **eslint:** 增加 'no-case-declarations': 'warn'[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([08e2b46](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/08e2b46ade750da82458707642d4cb546e04c5db))
* **file:** 更新pnpm-lock文件 ([b7aed24](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/b7aed246df4955c07883568d910d300a513aad67))
* **i18n:** 抽离监听国际化的方法[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([c4506b4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c4506b49d16f8e17f4629e1ddeee15c80226d035))
* **i18n:** 调整国际化，增加使用说明 ([65947d9](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/65947d92303588185715f8711fc58a56d4e96591))
* **i18n:** 调整i18n代码结构 ([26768ae](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/26768aee6fb6294c7462923af3a65c46b16c4c0d))
* **i18n:** 去除i18n打包配置，导致微应用异常[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([0bd0cec](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0bd0ceca303cbb87c460607bde6a96ad49b05ff1))
* **i18n:** 修复国际化问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([352b873](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/352b8734fa951dfabec3e611812f80f5d7c2dac1))
* **i18n:** 修复i18n旧模式下api兼容 ([450d2cf](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/450d2cf1a1f23ca02c85b871e2cf4436c25fafbd))
* **icon:** 更新图标文件 ([732a444](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/732a44439fce9a759abd67bb82ff8bbc56a73592))
* **image:** 删除无用文件 ([28659bb](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/28659bb098e260eee3fec6522782483d5defdf0b))
* **index:** 修复初始化配置设置 ([64d3c8e](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/64d3c8e813b53b99213fe23ddd0b3e08c2fa807d))
* **index:** 修复打包错误 ([cf96bfa](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/cf96bfa76a11751356fb44cea55b1edc412164d9))
* **je:** 调整je按需加载 ([6da9c79](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6da9c793339785e7f7dd7628224106fa0379e2ba))
* **layout:** 修改路由容器不支持滚动条 ([46a5b04](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/46a5b0413141d68e659cd32c63fa2c95f444fe04))
* **less:** 固定less版本3.0.4,防止主题构建有问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([03b82b6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/03b82b6cbeec0603824c19f9e2c81d3b76541408))
* **libs:** 更新libs项目[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([e0df5f6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e0df5f61d16a63e1b7ac29dfbf4f53b931991be0))
* **libs:** 更新libs项目[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([f8809bb](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f8809bb47660553bad5a9e69c45f05c68c4b69da))
* **lodash:** 修复lodash工具包依赖 ([9dd17e2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/9dd17e20446fa90b7503eed1fa4070c2b3d2c970))
* **lodash:** 增加lodash工具包 ([6c13199](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6c13199b0eae2484ba16d6fe4f1b6a5c6d246ad7))
* **login:** 抽离login方法，提供全局调用[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([729a2c5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/729a2c548229491a6a5242fb2798fcb6945f19a0))
* **login:** 微应用登录页丢失，默认使用系统登录[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([15735dd](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/15735dd58155ee5441fcae9fc266693abc398714))
* **login:** 修复登录成功后，路由跳转问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([a66b4c0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a66b4c0323c788c752ced8d837a83c891023b601))
* **login:** 修复登录后路由跳转问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([2ea0c62](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2ea0c62add33877bdc995bb27d9623019904c870))
* **login:** 修复登录相关操作[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([4747fcd](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4747fcd3b915c28606fc070649db627b0ed0953c))
* **login:** 修复用户失效，退出登录 ([2eed3ae](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2eed3ae3626937f7769284d7bf51897c8b41af1a))
* **login:** 修复主子应用交互混乱问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([e42b4ff](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e42b4ffda48cd7afaaeb3bf3fb4f4a87331e8b14))
* **login:** 优化登录页同主应用保持一致[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([39587c9](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/39587c989a192aebfa2d42d224fd4c9c9a19968c))
* **login:** 优化登录页同主应用保持一致[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([15cbd0e](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/15cbd0e5c1fea45fe97f874a11ab4ab733d628b1))
* **login:** 增加网站备案号 ([f8641f7](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f8641f71111a5cae697ca8398cd5f158e13f1142))
* **login:** 增加login样式 ([7a0db20](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/7a0db20513807519bfa04048074ffa3b639f1689))
* **logout:** 将logout从JE抽离到system ([abf67e0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/abf67e05098cc8f85c478294ca8de9c34a875dd0))
* **main:** 修复入口文件引入[[#2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/2)] ([f389efc](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f389efcf12c4b90d8160140136f02b14602bd724))
* **micro:** 调整微应用代码结构 ([91501cb](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/91501cb6a6024d28583e712cf5da1e4d382c3f8f))
* **micro:** 设置登录后的系统信息[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([8facb67](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/8facb67d32b666828fc06998ce44380a6694e8e9))
* **micro:** 设置子应用共享主应用系统数据[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([ebc8432](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ebc843288f4d94d8946022ed7b2de4de07b5f283))
* **micro:** 修复子应用注册登录成功事件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([f542360](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f542360d7f67dc2e3b3ad49f56b84dffd4b54fa4))
* **micro:** 修复microStorebug[[#45](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/45)] ([16dda0e](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/16dda0effefd2b376e533ad44d9961ebcde43916))
* **micro:** 优化主子应用交互方式 ([e4b9be6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e4b9be68f5b458310b2f5db509a0ac65865c298b))
* **micro:** 优化子应用打包[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([e3a93c2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e3a93c248e3d672dc532c42dfc41c8ee04d21226))
* **micro:** 增加微应用控制[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([bebda01](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/bebda019b930f198da1d354480a723a29efdf047))
* **micro:** 增加微应用控制负责度[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([7e9fd5f](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/7e9fd5fa3f4091a2298d0c35bbb5a25eb4a59673))
* **micro:** 增加微应用store的name属性[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([0ef5917](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0ef5917ed71601dcc559709e6b62ee90e33f44a4))
* **micro:** 增加主应用标识判断 ([a71ee5d](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a71ee5df08d2017d5adf2c198a096ac021dde80c))
* **micro:** 增加子应用激活路由配置[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([44ea958](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/44ea958233db96c2810d3a0a46619a441c0e44e4))
* **micro:** webpack打包主应用不处理output[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([f3dc8b1](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f3dc8b1dd5f3573fa4d4ce5efbb018ba42ca22c5))
* **mock:** 修复mock例子中的错误单词 ([c7f67f8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c7f67f8fc7f9de63b6e76d8e5a469a084643993a))
* **monaco:** 去除monaco打包插件，通过静态资源引用[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([dddb641](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/dddb641dd444d319df243ba5095be33057b54a3b))
* **mxgraph:** 修复源码，将所有属性注入到window，兼容沙箱模式[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([44b6b3f](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/44b6b3ffcf8dfdf4a7df8389373d5b489795dbc5))
* **npm:** 更新pnpm-lock文件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([ab75426](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ab7542619055c2599fe514ae398d52248ebbdfe4))
* **package:** 更新所有包的依赖[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([f60ecd3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f60ecd323b10533ad62bf689dff0c7aeb7a31a9b))
* **package:** 更新ant-design-vue版本[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([89efa95](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/89efa95a824e42ecfb4cbc2203e6b53c2e32b24c))
* **package:** 更新libs包[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([2c42413](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2c424139741cc8f559bdb74937a78b5c8477c20f))
* **pinyin:** 修复pinyin-pro依赖引用问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([ef71e00](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ef71e00761753aeba11233596c8dda6967d7d468))
* **plugin:** 增加plugin插件绑定JE[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([0caaf66](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0caaf6687fd90a50f7829f8372e23695d837e99d))
* **proxy:** 只有在主应用才会启用子应用的调试代理地址 ([8def26b](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/8def26be5a7add2cc7a3996796bc0be75ac140ee))
* **route:** 修复子应用菜单获取路由数据问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([4a61aff](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4a61aff54ea364a48a79b4e3831e65071bc1415a))
* **router:** 调整路由，路由守卫统一交由common管理[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([2bcc274](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2bcc27495ebc6f571a1827e89d29caa87ecd9c43))
* **router:** 支持系统路由自定义配置，支持路由菜单 ([bcb37f7](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/bcb37f77b2076b9d23ccfaf1f50cacaa5c373af9))
* **settings:** 调整系统设置按钮 ([41a8e2c](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/41a8e2c09c2562ba580ae62a177b25ace854bad6))
* **static:** 修复由于主题插件导致打包失败问题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([4f80a7e](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4f80a7ecb794eeac3efde7ea4bbb8179b184ba76))
* **style:** 调整系统滚动条的样式[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([2d849a5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2d849a583b44b82ba8ceea8b16a714b716c38448))
* **style:** 调整scroll，button样式 ([ba0aaca](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ba0aaca540345aade047b83300d629598ba93dfe))
* **style:** 开发模式下微应用不加载样式[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([19e0bd8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/19e0bd88ac37002f2e8e3631cd10f7b255a91f84))
* **style:** 修改style文件引用路径[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([e3df4bc](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e3df4bc77cdbd6444cc3f6d6b77cafbf0117ce6a))
* **style:** 优化ui库的资源位置 ([c2890e3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c2890e35fe680260c5623f41cbc95e7bf39b83d4))
* **style:** 增加ant tabs样式调整 ([bf61f69](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/bf61f694e5fdecbf033d239d6fd31e5aa24ba542))
* **style:** 组件包的样式引用修复 ([ef5fc45](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ef5fc455408a91e5a42b0e4377b2cb2606baee1c))
* **system:** 系统的方法改用@jecloud/utils里的initSystem[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([b00e589](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/b00e58957e732a252f23e550e5b01139f79c0bd8))
* **system:** 修改系统变量请求接口 ([b80e4e0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/b80e4e0bfa3dfa9a2988d51aa34ad1a88385aebf))
* **theme:** 表格主题色调整[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([2b80f27](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2b80f27e71f4b8082a1af54450b2905a14cee71d))
* **theme:** 调整表格主题色[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([ba82c8b](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ba82c8b22e7a46a851c39c939e340e2e892e9c6f))
* **theme:** 调整主题打包文件[[#6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/6)] ([b046aef](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/b046aefd0edd171fded0db61f33d7613d7eab051))
* **theme:** 将主题变量改为文件[[#7](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/7)] ([2c587f0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2c587f041935cff2ef7ed4b6b1cdb76284f5922a))
* **theme:** 确定主题支持方式[[#6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/6)] ([cb8d445](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/cb8d44556dde96e1ed7de7aa33f48f8ad4d9c028))
* **theme:** 修复代码构建主题文件目录错误 ([adf0bc4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/adf0bc472a68697dc338cc7beec2afbb19c18d9d))
* **theme:** 修复登录页的样式 ([5d416c4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/5d416c415d615fa1e66445d872a774d96c22435f))
* **theme:** 修复页面头部主题颜色 ([d9376c0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/d9376c060ce3cd383f53f39215e6238019be08a8))
* **theme:** 修复主题色样式 ([8383043](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/83830436cfb35570bf3a88667e269d86689db101))
* **theme:** 修复主题样式 ([a6fdef7](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a6fdef7afcf7becc1b109ea669db9e87c0d16d15))
* **theme:** 修复子应用不设置主题[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([90e4475](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/90e44752431eef8a828e509c540686f7ad0f2f06))
* **theme:** 修改系统主题默认字体色为[#3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)f3f3f ([c5f0e6d](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c5f0e6d17fc12ae7ca4ae14ecbd8cbfcfaad7306)), closes [#3f3f3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3f3f3)
* **theme:** 修改ant主题变量引用 ([c75eaa4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c75eaa4a87bf86f7e0362b2c2b5e28fc1a43b705))
* **theme:** 优化主题色选择组件 ([a645eff](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a645eff862ac9a71a0ec2084a62e6ceb7620c2de))
* **ui:** 绑定项目publishPath ([13f7d13](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/13f7d13cf32fdc156d5ac67e98039eea0e1aa1d1))
* **websocket:** 修复websocket监听方法[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([0183ee6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0183ee689d59f085c209e764512f7f0e94638372))
* **welcome:** 修复欢迎页的背景色和字体色[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([2ab3122](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2ab3122ee23db703b8d40ed28eeab8da81980c91))
* **workflow:** 移除@jecloud/workflow,增加@jecloud/plugin[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([e69cbe2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e69cbe20caa548085bcf74d28a83a911d4596673))


### Features

* **ajax:** 统一前后端数据格式 ([937fd40](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/937fd40adfcb81a15aecbf02087579fa56413a6f))
* **ant:** 更新antd版本3.0.0-beta.9 ([92a29e8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/92a29e8b69eb7a5c9797aa377fca8c3feba0fd85))
* **antd:** ant-design-vue升级到3.1.0正式版 ([5cd3f38](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/5cd3f380bd898364663e2905d8a85e86c630d689))
* **build:** 增加@jecloud/workflow样式[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([9b66b09](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/9b66b0909306d745bce60b9ab5170b7896d1eea5))
* **build:** 增加打包去除注释插件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([66282ff](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/66282ff1364b2d653d28f71e612f11c341ec612b))
* **build:** 增加微应用相关配置 ([137bc7f](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/137bc7f5b5173a66a1b43a81301cb904a6fcd05d))
* **build:** 增加文件配置和路由守卫自定义入口 ([751b820](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/751b8203845909638919e2852ecfbf6e42eeb0a3))
* **demo:** 调整入口文件，增加demo页面 ([0d13adb](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0d13adbb6a399296dca58d19550783214b0773c4))
* **doc:** 增加开发技巧等说明文档[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([06b95f5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/06b95f5c5dc6cd764587725e2d2f5265e7e5e420))
* **doc:** 增加开发技巧等说明文档[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([f412d5a](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f412d5ac1bddeabf7859265f4facd41785e122b3))
* **doc:** 增加git操作技巧[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([0eaf65d](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0eaf65d1c1cda085057cc566b73f3a7accbb60ce))
* **doc:** 增加websocket使用说明[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([6f0d2ca](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6f0d2ca85dc4cd573ca59c7ef436c8cd13156fbc))
* **fonts:** 增加字体图标[[#4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/4)] ([6e87e81](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6e87e81c1d19dd1905e5f522a75a515809912801))
* **func:** 增加jecloud/func功能包 ([2a24cb2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2a24cb2ee1a9bae835b06043edf3fdbdd5f5e3da))
* **func:** useSystem增加功能操作函数[[#45](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/45)] ([62502c1](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/62502c12bcd9e077b57e5bc2b6e400978bac170d))
* **i18n:** 增加国际化支持[[#3](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/3)] ([cfcc144](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/cfcc1448f85c829dbcb46d01b9f575362d3ea376))
* **icon:** 增加icons路由，去掉icons.html[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([7accc7d](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/7accc7d1fea492d0cbbd86b47c609388409abe69))
* **icons:** 增加图标使用帮助[[#4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/4)] ([e7b5895](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e7b589560f172b718cd7be07b34f061d8b84fcdf))
* **icons:** 增加jeicons图标 ([37c1055](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/37c105591bb681c37725774048244ddfd085657b))
* **je:** 功能组件绑定JE对象，用于事件操作[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([4efc10c](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4efc10cde1099b2e88d49717d960c628295ee376))
* **je:** 混入JE系统方法useSystem[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([eb627a2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/eb627a2e773b4a7d220260701a170e8703c9903c))
* **je:** 增加watchWebSocket方法，只有主应用可用[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([20e0a0e](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/20e0a0e1a897fb2b1eb488eeb8cd51e794982033))
* **login:** 优化登录操作[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([45b29d2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/45b29d24bba9930bee9a1cf6b9c5b41e83a6519d))
* **login:** login增加部门选择[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([770df7e](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/770df7ea8a47ce2881b06ed2c0bb2ff9fbfa074c))
* **micro:** 暴露micro-app的钩子函数[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([59e609d](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/59e609dbc541d7e15f3e6a56dd347309c67a5cc4))
* **micro:** 调整微应用框架qiankun改为micro-app[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([23630f2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/23630f21e7c79f380806caf9ac8424e9f7f9b9c3))
* **micro:** 微应用支持触发其他微应用事件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([0c54bf7](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0c54bf77ae1733b1cccef5d2b45419bd31269b3b))
* **micro:** 增加主子应用通讯[[#7](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/7)] ([2d7b3d2](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/2d7b3d2fd72563184e8adadcc78abe90175521b2))
* **monaco:** 增加monaco静态资源，不再单独打包[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([c0961b0](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/c0961b04dd161afc33a9e48a4baaf5e111888bee))
* **monaco:** 增加webpack，vite的monaco插件 ([1df85bc](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/1df85bcf34dd7a68888de72026762dc6d05cf99d))
* **monaco:** 重写vite插件，修改路径引用错误问题 ([dea79f1](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/dea79f1790d42e4e896636112319ae9f4627b1e9))
* **mork:** 增加mork数据支持 ([3eb1d44](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/3eb1d449abfea042263f3d42f091c001d23138c7))
* **mxgraph:** 增加mxgraph静态资源文件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([e4cdc85](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/e4cdc853b6db5d8f34ab2bd3c34a5ef1f07f766a))
* **plan:** 增加globalStore的方案配置[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([a335bb5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a335bb5c9eeadcec12f55e17a97a1bb8ce0422b4))
* **preview:** 增加本地预览服务 ([0cd7aa1](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/0cd7aa141fc756e900323fb142fe62d69271844f))
* **preview:** 增加本地ip输出，方便调试 ([4afdc69](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/4afdc69fec00fbb0ff1daf85a368be9cec75c83d))
* **rbac:** 更新登录和获取用户接口[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([83a9597](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/83a9597c021c1c91e21f2e77a4614a675aaa0186))
* **router:** 支持用户自定义history ([1421921](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/142192104955f238e8112d959c1d55a35f1318fd))
* **router:** 支持自定义路由白名单[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([1ee13a4](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/1ee13a47dd250e2ea4ca2a3367d8bb455388a078))
* **static:** 静态资源文件提取[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([f3e0edb](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/f3e0edbbe6a20f33922449e56db057e37b777396))
* **static:** 增加静态资源包文件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([28e62e5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/28e62e5b23fa933a368473623c2d152fad5e5d7a))
* **theme:** 增加灰色模式，弱色模式[[#6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/6)] ([fa4d1df](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/fa4d1df85b69176cb5956e52ac77594b5f8307d2))
* **theme:** 增加主题支持[[#6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/6)] ([bbfa033](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/bbfa0333e212e51b73355b4bb948f41933c5064b))
* **theme:** 增加vben主题设置[[#6](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/6)] ([ad34b2e](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ad34b2ebcb496b8e422421bd940af99ab4666798))
* **theme:** 主题组件增加切换主题事件[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([ef9ac87](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/ef9ac87c6ae84c2f14de177d0ed89312ca7a0f30))
* **theme:** vuecli支持主题配置[[#7](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/7)] ([b361a5f](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/b361a5f0e2e976884a25e4494d2fa003b8a583ce))
* **tinymce:** 增加tinymce插件，更新antd版本 ([13f390a](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/13f390a11f4d131177ab94cde91ef694ace3a2a5))
* **version:** release v1.0.0 ([28d5638](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/28d5638d8cc5361db1c892e069306a2092f85d5c))
* **version:** release v1.0.0 ([a969009](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/a96900905fea3ce679206313ddd57c2ec0504011))
* **workflow:** 增加@jecloud/worfkow[[#8](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/issues/8)] ([6d1aea5](https://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-boards/commit/6d1aea5c6719b6115ffe84a89c6f9b0c28861267))



