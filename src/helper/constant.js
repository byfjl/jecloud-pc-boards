/**
 * 系统常量配置文件，命名规则
 * 平台核心：JE_模块_变量名
 * 业务常量：业务(MENU)_模块_变量名
 */
//系统字段解析内容
export const RESOURCE_FIELD_DATA = [
  {
    type: '系统',
    name: '主键ID',
    code: '资源表编码_ID',
    remark: '系统默认为表生成的主键，此主键字段不能修改',
  },
  {
    type: '系统',
    name: '数据状态',
    code: 'SY_STATUS',
    remark: '数据状态，默认数据是否启用',
  },
  {
    type: '系统',
    name: '排序字段',
    code: 'SY_ORDERINDEX',
    remark: '排序字段',
  },
  {
    type: '系统',
    name: '登记部门主键',
    code: 'SY_CREATEORGID',
    remark: '创建人所属部门主键',
  },
  {
    type: '系统',
    name: '登记部门',
    code: 'SY_CREATEORGNAME',
    remark: '创建人所属部门名称',
  },
  {
    type: '系统',
    name: '登记时间',
    code: 'SY_CREATETIME',
    remark: '创建时间/登记时间',
  },
  {
    type: '系统',
    name: '登记人主键',
    code: 'SY_CREATEUSERID',
    remark: '创建人主键',
  },
  {
    type: '系统',
    name: '登记人',
    code: 'SY_CREATEUSERNAME',
    remark: '创建人名称',
  },
  {
    type: '系统',
    name: '修改人部门主键',
    code: 'SY_MODIFYORGID',
    remark: '修改人所属部门主键',
  },
  {
    type: '系统',
    name: '修改人部门',
    code: 'SY_MODIFYORGNAME',
    remark: '修改人所属部门名称',
  },
  {
    type: '系统',
    name: '修改人主键',
    code: 'SY_MODIFYUSERID',
    remark: '修改人主键',
  },
  {
    type: '系统',
    name: '修改人',
    code: 'SY_MODIFYUSERNAME',
    remark: '修改人名称',
  },
  {
    type: '系统',
    name: '修改时间',
    code: 'SY_MODIFYTIME',
    remark: '修改时间',
  },
  {
    type: '系统',
    name: '所属公司ID',
    code: 'SY_COMPANY_ID',
    remark: '所属公司ID',
  },
  {
    type: '系统',
    name: '所属公司名称',
    code: 'SY_COMPANY_NAME',
    remark: '所属公司名称',
  },
  {
    type: '系统',
    name: '所属集团公司ID',
    code: 'SY_GROUP_COMPANY_ID',
    remark: '所属集团公司ID',
  },
  {
    type: '系统',
    name: '所属集团公司名称',
    code: 'SY_GROUP_COMPANY_NAME',
    remark: '所属集团公司名称',
  },
  {
    type: '系统',
    name: '所属机构ID',
    code: 'SY_ORG_ID',
    remark: '所属机构ID',
  },
  {
    type: '系统',
    name: '租户ID',
    code: 'SY_TENANT_ID',
    remark: '所属租户ID',
  },
  {
    type: '系统',
    name: '租户名称',
    code: 'SY_TENANT_NAME',
    remark: '所属租户名称',
  },

  {
    type: '系统',
    name: '节点名称',
    code: '表后缀_TEXT',
    remark: '树形节点名称',
  },
  {
    type: '系统',
    name: '节点编码',
    code: '表后缀_CODE',
    remark: '树形节点编码',
  },
  {
    type: '系统',
    name: '父节点ID',
    code: 'SY_PARENT',
    remark: '树形父节点ID',
  },
  {
    type: '系统',
    name: '节点类型',
    code: 'SY_NODETYPE',
    remark: '树形节点类型，主要有ROOT（根节点）、GENERAL（文件夹）、LEAF（叶子节点）',
  },
  {
    type: '系统',
    name: '父节点路径',
    code: 'SY_PARENTPATH',
    remark: '树形父节点路径，路径形如父节点ID/当前节点ID',
  },
  {
    type: '系统',
    name: '层次',
    code: 'SY_LAYER',
    remark: '当前节点所属树形层级',
  },
  {
    type: '系统',
    name: '树形路径',
    code: 'SY_PATH',
    remark: '当前节点树形路径，参考父节点路径',
  },
  {
    type: '系统',
    name: '是否启用',
    code: 'SY_DISABLED',
    remark: '当前节点是否启用',
  },
  {
    type: '系统',
    name: '树形排序字段',
    code: 'SY_TREEORDERINDEX',
    remark: '当前节点树形排序，排序规则为父节点排序/当前节点排序，其中排序为6位数字组合',
  },
  {
    type: '系统',
    name: '扩展字段01-10',
    code: 'SY_EXTEND01-SY_EXTEND10',
    remark: '扩展字段，此字段一般给与终端用户使用，业务开发人员也可以使用。',
  },

  {
    type: '工作流',
    name: '审核标记',
    code: 'SY_AUDFLAG',
    remark: '当前流程状态',
  },
  {
    type: '工作流',
    name: '流程实例ID',
    code: 'SY_PIID',
    remark: '流程实例ID',
  },
  {
    type: '工作流',
    name: '流程定义ID',
    code: 'SY_PDID',
    remark: '流程定义ID',
  },
  {
    type: '工作流',
    name: '流程启动人ID',
    code: 'SY_STARTEDUSER',
    remark: '流程启动人ID',
  },
  {
    type: '工作流',
    name: '流程启动人',
    code: 'SY_STARTEDUSERNAME',
    remark: '流程启动人名称',
  },
  {
    type: '工作流',
    name: '流程已执行人ID',
    code: 'SY_APPROVEDUSERS',
    remark: '流程已执行人ID，多个人用逗号隔离',
  },
  {
    type: '工作流',
    name: '流程已执行人',
    code: 'SY_APPROVEDUSERNAMES',
    remark: '流程已执行人名称，多个人用逗号隔离',
  },
  {
    type: '工作流',
    name: '流程待执行人ID',
    code: 'SY_PREAPPROVUSERS',
    remark: '流程当前处理人ID，多个人用逗号隔离',
  },
  {
    type: '工作流',
    name: '流程待执行人',
    code: 'SY_PREAPPROVUSERNAMES',
    remark: '流程当前处理人名称，多个人用逗号隔离',
  },
  {
    type: '工作流',
    name: '流程任务指派人ID',
    code: 'SY_LASTFLOWUSERID',
    remark: '当前任务上一任务节点处理人ID，多个人用逗号隔离',
  },
  {
    type: '工作流',
    name: '流程任务指派人',
    code: 'SY_LASTFLOWUSER',
    remark: '当前任务上一任务节点处理人名称，多个人用逗号隔离',
  },
  {
    type: '工作流',
    name: '当前执行节点',
    code: 'SY_CURRENTTASK',
    remark: '流程当前执行节点',
  },
];

//首页内容分类-流程
export const HOME_CONTENT_FLOW = 'HOME_CONTENT_FLOW';
//首页内容分类-通知
export const HOME_CONTENT_NOTIFY = 'HOME_CONTENT_NOTIFY';
//首页内容分类-微邮
export const HOME_CONTENT_MICROMAIL = 'HOME_CONTENT_MICROMAIL';
//首页内容分类-批注
export const HOME_CONTENT_NOTE = 'HOME_CONTENT_NOTE';

//切换流程类型事件
export const HOME_FLOW_SWITCH = 'HOME_FLOW_SWITCH';
//流程审核状态
export const HOME_FLOW_CHECK = 'HOME_FLOW_CHECK';
//流程发起状态
export const HOME_FLOW_START = 'HOME_FLOW_START';

//流程子节点切换事件
export const HOME_FLOW_CHILD_SWITCH = 'HOME_FLOW_CHILD_SWITCH';
//流程-待我审批
export const HOME_FLOW_WAIT_EXAMINE = 'HOME_FLOW_WAIT_EXAMINE';
//流程-已办理
export const HOME_FLOW_ALREADY_TRANSACT = 'HOME_FLOW_ALREADY_TRANSACT';
//流程-我的流程
export const HOME_FLOW_MINE_FLOW = 'HOME_FLOW_MINE_FLOW';
//流程-延时处理
export const HOME_FLOW_DALY_DISPOSE = 'HOME_FLOW_DALY_DISPOSE';
//流程-审批告知
export const HOME_FLOW_EXAMINE_NOTIFY = 'HOME_FLOW_EXAMINE_NOTIFY';
//流程-完结状态-完成
export const HOME_FLOW_STATUS_FINISH = 'HOME_FLOW_STATUS_FINISH';
//流程-完结状态-未完成
export const HOME_FLOW_STATUS_UNFINISH = 'HOME_FLOW_STATUS_UNFINISH';
//流程数量监听
export const HOME_FLOW_EVENT_COUNT = 'HOME_FLOW_EVENT_COUNT';
//流程发起分类变化
export const HOME_FLOW_START_TYPE = 'HOME_FLOW_START_TYPE';
//流程审核分类变化
export const HOME_FLOW_CHECK_TYPE = 'HOME_FLOW_CHECK_TYPE';

//通知-未读
export const HOME_NOTIDY_UNREAD = 'HOME_NOTIDY_UNREAD';
//通知-已读
export const HOME_NOTIDY_READ = 'HOME_NOTIDY_READ';
//通知-全部
export const HOME_NOTIDY_ALL = 'HOME_NOTIDY_ALL';

//微邮-我收到的
export const HOME_MICROMAIL_RECEIVE = 'HOME_MICROMAIL_RECEIVE';
//微邮-我发出的
export const HOME_MICROMAIL_SEND = 'HOME_MICROMAIL_SEND';

//批注-未读
export const HOME_NOTE_UNREAD = 'HOME_NOTE_UNREAD';
//批注-已读
export const HOME_NOTE_READ = 'HOME_NOTE_READ';
//批注-全部
export const HOME_NOTE_ALL = 'HOME_NOTE_ALL';

//日记-收藏
export const HOME_DIRAY_COLLECT = 'HOME_DIRAY_COLLECT';
//日记-共享
export const HOME_DIRAY_SHARE = 'HOME_DIRAY_SHARE';
//日记-共享给我
export const HOME_DIRAY_SHARE_GIVE = 'HOME_DIRAY_SHARE_GIVE';
//日记-我的共享
export const HOME_DIRAY_SHARE_SEND = 'HOME_DIRAY_SHARE_SEND';

//创建日程弹窗事件
export const HOME_CALENDAR_CREATE_VISIBLE = 'HOME_CALENDAR_CREATE_VISIBLE';
//编辑日程弹窗事件
export const HOME_CALENDAR_EDIT_VISIBLE = 'HOME_CALENDAR_EDIT_VISIBLE';
//日程弹窗关闭事件
export const HOME_CALENDAR_CLOSE_MODAL = 'HOME_CALENDAR_CLOSE_MODAL';
//查看日程事件
export const HOME_CALENDAR_TASK_VISIBLE = 'HOME_CALENDAR_TASK_VISIBLE';
//查看日程列表事件
export const HOME_CALENDAR_LIST_VISIBLE = 'HOME_CALENDAR_LIST_VISIBLE';
//关闭日程列表弹窗
export const HOME_CALENDAR_LIST_CLOSE = 'HOME_CALENDAR_LIST_CLOSE';
//日程列表编辑弹窗
export const HOME_CALENDAR_LIST_EDIT = 'HOME_CALENDAR_LIST_EDIT';
//日程更新成功事件
export const HOME_CALENDAR_UPDATE_SUCCESS = 'HOME_CALENDAR_UPDATE_SUCCESS';
//日程列表弹窗打开
export const HOME_CALENDAR_OPEN_LIST = 'HOME_CALENDAR_OPEN_LIST';

//通知分类选择事件
export const HOME_NOTIFY_SWITCH_TYPE = 'HOME_NOTIFY_SWITCH_TYPE';
//通知模块未读消息数量
export const HOME_NOTIFY_UNREAD_COUNT = 'HOME_NOTIFY_UNREAD_COUNT';

//收藏 区域高度变化
export const HOME_DIRAY_COLLECT_HEIGHT = 'HOME_DIRAY_COLLECT_HEIGHT';
//共享 区域高度变化
export const HOME_DIRAY_SHARE_HEIGHT = 'HOME_DIRAY_SHARE_HEIGHT';

//流程发起对应按钮颜色
export const MENU_CORE_BGCOLOR = [
  { code: '#38BA72', text: '绿色' },
  { code: '#52C6C6', text: '蓝色' },
  { code: '#ED5858', text: '红色' },
  { code: '#F28D48', text: '橙色' },
  { code: '#9382FF', text: '紫色' },
  { code: '#F5D540', text: '黄色' },
];
//文件类型
export const FILE_TYPES = [
  {
    title: 'Word',
    suffixs: ['doc', 'docx', 'dot', 'dotx', 'docm'],
    class: 'fas fa-file-word',
    color: '#0057aa',
  },
  {
    title: 'Excel',
    suffixs: ['xls', 'xlsx', 'xlsm'],
    class: 'fas fa-file-excel',
    color: '#00843E',
  },
  {
    title: 'PPT',
    suffixs: ['pptx', 'pptm', 'ppt', 'potx', 'potm', 'pot', 'ppsx', 'ppsm', 'xml'],
    class: 'fas fa-file-powerpoint',
    color: '#f34e19',
  },
  {
    title: 'PDF',
    suffixs: ['pdf'],
    class: 'fas fa-file-powerpoint',
    color: '#e52c0e',
  },
  {
    title: 'Image',
    suffixs: ['jpg', 'png', 'gif', 'svg', 'jpeg'],
    class: 'fas fa-file-image',
    color: '#ff7744',
  },
  {
    title: 'Video',
    suffixs: [
      'rmvb',
      'mv',
      'avi',
      'mtv',
      'rm',
      '3gp',
      'amv',
      'flv',
      'dmv',
      'mp4',
      'mpeg1',
      'mpeg2',
      'mpeg4',
    ],
    class: 'fas fa-file-video',
    color: '#ff7744',
  },
  {
    title: 'Audio',
    suffixs: ['mp3', 'wma', 'wav', 'ape', 'mid'],
    class: 'fas fa-file-video',
    color: '#ff7744',
  },
  {
    title: 'ZIP',
    suffixs: ['zip', '7-zip', '7z', 'winzip'],
    class: 'fas fa-file-archive',
    color: '#fbaf32',
  },
];
//未知类型文件
export const FILE_UNKNOWN = {
  title: '未知',
  class: 'fas fa-file-exclamation',
  color: '#a9aba9',
};
