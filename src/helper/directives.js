const directives = {
  resize: {
    mounted(el, binding, vnode) {
      let width = '',
        height = '';
      function isReize() {
        const style = document.defaultView.getComputedStyle(el);
        if (width !== style.width || height !== style.height) {
          binding.value({ width: style.width, height: style.height }); // 关键(这传入的是函数,所以执行此函数)
        }
        width = style.width;
        height = style.height;
      }
      el.__vueSetInterval__ = setInterval(isReize, 10);
    },
    beforeUnmount(el) {
      clearInterval(el.__vueSetInterval__);
    },
  },
  scrollBottom: {
    mounted(el, binding, vnode) {
      el.addEventListener('scroll', function () {
        const CONDITION = el.scrollHeight - el.scrollTop <= el.clientHeight;
        if (CONDITION) {
          binding.value();
        }
      });
    },
  },
};
export default directives;
