import { h, ref, toRaw } from 'vue';
import { Modal } from '@jecloud/ui';
import flowMain from '@/views/board-home/components/flow-main.vue';
import notifyMain from '@/views/board-home/components/notify-main.vue';
export const showModal = ({ type, props = {} }) => {
  let title = '流程';
  switch (type) {
    case 'WF': //流程
      title = '流程';
      break;
    case 'MSG': //通知
      title = '通知';
      break;
    default:
      break;
  }
  Modal.window({
    title: `${title}`,
    headerStyle: { height: '50px' },
    bodyStyle: { height: '100%', padding: '0px' },
    content() {
      switch (type) {
        case 'WF': //流程
          return h(flowMain, { type, ...props });
        case 'MSG': //通知
          return h(notifyMain, { type, ...props });
        default:
          return null;
      }
    },
  });
};
