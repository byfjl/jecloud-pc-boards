/**
 *
 * @param {*} arr 原数组
 * @param {*} a 某个对象当前位置
 * @param {*} b 某个对象想要移动到的位置
 * @returns 移动位置后新数组
 */
export function moveLocation(arr, a, b) {
  let arr_temp = [].concat(arr);
  arr_temp.splice(b, 0, arr_temp.splice(a, 1)[0]);
  return arr_temp;
}
