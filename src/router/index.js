import baseRoutes from '@common/router/routes';
import { t } from '@/locales';
import Home from '../views/index.vue';
/**
 * 基础路由
 * 如果有业务需要，可以自行调整
 * 固定路由：Home(/),Login(/login)
 */
const routes = [
  {
    path: '/',
    name: 'Home',
    text: t('menu.home'),
    // menu: true,
    // component: Home,
    redirect: '/boardHome',
  },
  {
    path: '/boardResource',
    text: t('resourceBoard.resource'),
    menu: true,
    name: 'BoardResource',
    component: () => import('@/views/board-resource/index.vue'),
  },
  {
    path: '/boardDevelop',
    text: t('developBoard.develop'),
    menu: true,
    name: 'BoardDevelop',
    component: () => import('@/views/board-develop/index.vue'),
  },
  {
    path: '/boardManage',
    text: t('manageBoard.manage'),
    menu: true,
    name: 'BoardManage',
    component: () => import('@/views/board-manage/index.vue'),
  },
  {
    path: '/boardWork',
    text: t('workBoard.work'),
    menu: true,
    name: 'BoardWork',
    component: () => import('@/views/board-work/index.vue'),
  },
  {
    path: '/boardHome',
    text: t('homeBoard.home'),
    menu: true,
    name: 'BoardHome',
    component: () => import('@/views/board-home/index.vue'),
  },
  {
    path: '/boardInit',
    text: t('bomeBoard.init'),
    menu: true,
    name: 'BoardInit',
    component: () => import('@/views/board-init/index.vue'),
  },
  ...baseRoutes,
];
export default routes;

/**
 * 自定义路由History
 * @returns History
 */
// export function createRouterHistory() {
//   return createMemoryHistory();
// }

/**
 * 自定义路由守卫
 */
// export function createRouterGuard(router) {
//   // 业务逻辑
// }

/**
 * 路由白名单
 */
//  export const whiteRoutes = [];
