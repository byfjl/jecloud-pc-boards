/**
 * API_URL命名规则：API_模块_方法
 */
// 获取资源表首页模版数据（资源总数、类型分布、产品分布）
export const API_Board_Resource = '/je/meta/resourceTable/table/getHomePageInfo';
// 获取开发门户平台数据
export const API_Board_Develop = '/je/meta/develop/getCoreResourceCount';
// 组织在职信息统计
export const API_Board_StaffStatistic = '/je/rbac/cloud/account/staffStatistic';
// 获取当月登录人次统计
export const API_Board_LoginStatistic = '/je/rbac/cloud/account/loginStatistic';
// 获取2小时内活跃人员
export const API_Board_TwoHoursActive = '/je/rbac/cloud/log/twoHoursActive';
// 获取登录详情记录
export const API_Board_LoginLogList = '/je/rbac/cloud/log/loginLogList';
// 获取访问详情记录
export const API_Board_BuryList = '/je/rbac/cloud/log/buryList';
// 获取首页常用菜单
export const API_Board_HomeCommon = '/je/rbac/commonMenu/loadMenu';
//按用户id和时间加载日程信息
export const API_Board_CalendarSchedule = '/je/meta/calendar/load';
//获取消息提醒类型字典
export const API_Board_GetMsDiyDicItem = '/je/meta/calendar/getMsDiyDicItem';
//保存新建的日程
export const API_Board_SaveCalendar = '/je/meta/calendar/doSave';
//删除日程
export const API_Board_RemoveCalendar = '/je/meta/calendar/doRemove';
//删除日程
export const API_Board_UpdateCalendar = '/je/meta/calendar/doUpdate';
//获取我的日程任务树
export const API_Board_GetMyTaskTree = '/je/meta/calendar/getMyTaskTree';
//获取公告菜单
export const API_Board_NewsMenu = '/je/meta/dictionary/getDicItemByCode';
//根据新闻code获取对应的新闻列表
export const API_Board_NewsList = '/je/meta/notice/load';
//新闻点赞/阅读
export const API_Board_NoticeUpdate = '/je/meta/notice/doUpdate';
//从字典中获取对应分类
export const API_Board_GetDicItemByCode = '/je/meta/dictionary/getDicItemByCode';
//按消息类型获取通知列表
export const API_Board_GetLoadUserMsg = '/je/message/homePortal/loadUserMsg';
//通知消息更改已读状态
export const API_Board_UpdateReadStatus = '/je/message/homePortal/doUpdateOneSign';
//通知消息更改全部已读状态
export const API_Board_UpdateAllReadStatus = '/je/message/homePortal/readAllUserMsg';
//按关键字搜索通知分类
export const API_Board_QueryNotifyType = '/je/message/homePortal/loadMsgType';
//收藏列表数据
export const API_Board_GetCollectData = '/je/message/myCollection/loadGridCollection';
//首页收藏列表 单个、全部删除
export const API_Board_DeleteCollect = '/je/message/myCollection/deleteCollection';
//分享给我的 列表
export const API_Board_GetShareMyData = '/je/message/myShare/loadShareMe';
//删除 分享给我的
export const API_Board_DeleteShareMe = '/je/message/myShare/deleteShareMe';
//分享给我的 修改阅读状态
export const API_Board_UpdateShareMeRead = '/je/message/myShare/updateShareMeRead';
//获取我的分享 列表
export const API_Board_GetMyShareData = '/je/message/myShare/loadMyShare';
//删除 我的分享
export const API_Board_DeleteMyShare = '/je/message/myShare/deleteMyShare';
//流程分类
export const API_Board_GetLoadTree = '/je/meta/dictionary/tree/loadTree';
//获取首页待办信息
export const API_Board_GetTask = '/je/workflow/currentUserTask/getTask';
//流程 加入标签
export const API_Board_AddCollect = '/je/workflow/currentUserTask/collect';
//流程 加入标签
export const API_Board_DelayManage = '/je/workflow/currentUserTask/delay';
//流程 发起列表
export const API_Board_GetInitiateList = '/je/workflow/currentUserTask/getInitiateList';
//开发 获取开发者信息
export const API_Board_GetDevelopUser = '/je/meta/develop/getDevelopUser';
//开发 获取开发者日志信息
export const API_Board_LoadDevelopUserLog = '/je/meta/develop/loadDevelopUserLog';
//审批公告已读
export const API_Board_UpdateExamineRead = '/je/workflow/currentUserTask/setAsRead';
