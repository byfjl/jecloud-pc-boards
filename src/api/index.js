/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  API_Board_Resource,
  API_Board_Develop,
  API_Board_StaffStatistic,
  API_Board_LoginStatistic,
  API_Board_TwoHoursActive,
  API_Board_LoginLogList,
  API_Board_BuryList,
  API_Board_HomeCommon,
  API_Board_CalendarSchedule,
  API_Board_GetMsDiyDicItem,
  API_Board_SaveCalendar,
  API_Board_RemoveCalendar,
  API_Board_UpdateCalendar,
  API_Board_NewsMenu,
  API_Board_NewsList,
  API_Board_NoticeUpdate,
  API_Board_GetDicItemByCode,
  API_Board_GetLoadUserMsg,
  API_Board_UpdateReadStatus,
  API_Board_UpdateAllReadStatus,
  API_Board_QueryNotifyType,
  API_Board_GetCollectData,
  API_Board_DeleteCollect,
  API_Board_GetShareMyData,
  API_Board_DeleteShareMe,
  API_Board_UpdateShareMeRead,
  API_Board_GetMyShareData,
  API_Board_DeleteMyShare,
  API_Board_GetLoadTree,
  API_Board_GetTask,
  API_Board_AddCollect,
  API_Board_DelayManage,
  API_Board_GetInitiateList,
  API_Board_GetDevelopUser,
  API_Board_LoadDevelopUserLog,
  API_Board_UpdateExamineRead,
} from './urls';

/**
 * 获取资源表数据
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetResourceData() {
  return ajax({ url: API_Board_Resource }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取开发门户平台数据
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetPlatformData() {
  return ajax({ url: API_Board_Develop }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取组织在职信息统计
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetStaffStatistic(params) {
  return ajax({ url: API_Board_StaffStatistic, method: 'GET', params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取当月登录人次统计
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetLoginStatistic(params) {
  return ajax({ url: API_Board_LoginStatistic, method: 'GET', params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取2小时内活跃人员数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetTwoHoursActive(params) {
  return ajax({ url: API_Board_TwoHoursActive, method: 'GET', params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取登录详情记录
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetLoginLogList(params) {
  return ajax({ url: API_Board_LoginLogList, method: 'GET', params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取访问记录详情
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetBuryList(params) {
  return ajax({ url: API_Board_BuryList, method: 'GET', params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取首页常用菜单
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetBoardHomeCommon(params) {
  return ajax({ url: API_Board_HomeCommon, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取首页日程信息
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetCalendarSchedule(params) {
  return ajax({ url: API_Board_CalendarSchedule, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取消息提醒类型
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetMsDiyDicItem(params) {
  return ajax({ url: API_Board_GetMsDiyDicItem, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 保存创建的日程
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function SaveCalendar(params) {
  return ajax({ url: API_Board_SaveCalendar, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 删除日程
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function RemoveCalendar(params) {
  return ajax({ url: API_Board_RemoveCalendar, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 删除日程
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function UpdateCalendar(params) {
  return ajax({ url: API_Board_UpdateCalendar, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取公告菜单
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetNewsMenu(params) {
  return ajax({ url: API_Board_NewsMenu, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 根据新闻code获取对应的新闻列表
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetNewsList(params) {
  return ajax({ url: API_Board_NewsList, params, headers: { pd: 'meta' } }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 操作新闻状态（点赞/阅读）
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function UpdateNoticeStatus(params) {
  return ajax({ url: API_Board_NoticeUpdate, params, headers: { pd: 'meta' } }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 从字典中获取对应分类
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetDicItemByCode(params) {
  return ajax({ url: API_Board_GetDicItemByCode, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取通知模块信息
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetLoadUserMsg(params) {
  return ajax({ url: API_Board_GetLoadUserMsg, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 更改通知消息已读状态
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function UpdateReadStatus(params) {
  return ajax({ url: API_Board_UpdateReadStatus, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 更改通知消息全部已读状态
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function UpdateAllReadStatus(params) {
  return ajax({ url: API_Board_UpdateAllReadStatus, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 按关键字搜索通知分类
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function QueryNotifyType(params) {
  return ajax({ url: API_Board_QueryNotifyType, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 获取收藏列表
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetCollectData(params) {
  return ajax({ url: API_Board_GetCollectData, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 首页收藏列表 单个、全部删除
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function DeleteCollect(params) {
  return ajax({ url: API_Board_DeleteCollect, params, headers: { pd: 'message' } }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 分享给我 数据列表
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetShareMyData(params) {
  return ajax({ url: API_Board_GetShareMyData, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 删除 分享给我的
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function DeleteShareMe(params) {
  return ajax({ url: API_Board_DeleteShareMe, params, headers: { pd: 'message' } }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 分享给我的 修改阅读状态
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function UpdateShareMeRead(params) {
  return ajax({ url: API_Board_UpdateShareMeRead, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 获取我的分享 数据列表
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetMyShareData(params) {
  return ajax({ url: API_Board_GetMyShareData, params, headers: { pd: 'message' } }).then(
    (info) => {
      if (info) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}
/**
 * 删除 我的分享
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function DeleteMyShare(params) {
  return ajax({ url: API_Board_DeleteMyShare, params, headers: { pd: 'message' } }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取流程分类
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetLoadTree(params) {
  return ajax({ url: API_Board_GetLoadTree, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取首页对应流程列表
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetTask(params) {
  return ajax({ url: API_Board_GetTask, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 流程加入标签
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function AddCollect(params) {
  return ajax({ url: API_Board_AddCollect, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 流程 延时处理
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function DelayManage(params) {
  return ajax({ url: API_Board_DelayManage, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 流程发起列表
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetInitiateList(params) {
  return ajax({ url: API_Board_GetInitiateList, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取开发者信息
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function getDevelopUser(params) {
  return ajax({ url: API_Board_GetDevelopUser, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取开发者日志信息
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function getDevelopUserLog(params) {
  return ajax({ url: API_Board_LoadDevelopUserLog, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 审批公告更改已读状态
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function updateExamineRead(params) {
  return ajax({ url: API_Board_UpdateExamineRead, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
