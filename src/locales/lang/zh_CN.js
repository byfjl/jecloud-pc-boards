/**
 * 1. 通过键值形式定义语言
 * 2. 通过useI18n().t() 展示语言
 */
export default {
  resourceBoard: {
    resource: '资源表',
    title: '欢迎来到资源表的世界！',
    resourceTotal: '资源总数',
    typeDistribute: '类型分布',
    productDistribute: '产品分布',
    explain: {
      explainTitle: '名词解释',
      general: '【普通表】',
      generalContent: '：数据库中的表。平台默认会增加系统字段，默认以SY_开头。',
      simple: '【单根树形表】',
      simpleContent:
        '：具有树形结构的表，单根是指表数据中只有一个ROOT节点，多用于主功能，例如：菜单管理。',
      dorgan: '【多根树形表】',
      dorganContent:
        '：具有树形结构的表，多根是指表数据中包含多个ROOT节点，多根树多用于子功能，例如：数据字典的子功能（字典项）。',
      view: '【视图】',
      viewContent: '：数据库视图。',
      import: '【导入表】',
      importContent: '：数据库中已有的表，导入到平台中进行统一管理。',
      relation: '【关系视图】',
      relationContent: '：展示本表的父表、子表信息，以及基于本表创建的功能信息。',
    },
    standard: {
      standardTitle: '命名规范',
      resource: '资源表命名规范（官方推荐）',
      resourceTier:
        '命名规范分为三段。例如：OA产品下有流程审批模块，模块下有请假申请功能。表名为：OA_LCSP_QJSQ。',
      table: '表格列命名规范（官方推荐）',
      tableTier:
        '命名规范分为两段。例如：OA产品的流程审批模块的出差申请功能的部门。列数据列名为：CCSQ_BM。',
      view: '视图命名规范（官方推荐）',
      viewTier:
        '命名规范分为四段。例如：OA产品的会议管理模块的“我的会议”功能。视图名为：V_OA_HYGL_WDHY。',
    },
    field: {
      title: '系统字段解析',
    },
  },
  developBoard: {
    develop: '开发',
    fastEntry: '快捷入口',
    dataDiction: '数据字典',
    resource: '资源表',
    subsystem: '应用中心',
    menu: '菜单管理',
    workflow: '工作流引擎',
    process: '流程监控',
    platform: '平台设置',
    system: '系统设置',
    platformData: '平台数据',
    productData: '产品',
    resourceData: '资源表',
    menuData: '菜单',
    funcData: '功能',
    dictionData: '数据字典',
    workflowData: '工作流',
    log: '开发者操作日志',
    logDetail: '操作日志详情',
    prev: '上一页',
    next: '下一页',
    go: '前往',
    page: '页',
  },
  manageBoard: {
    manage: '管理',
    activeRecord: '活跃人员',
    loginRecord: '登录详情',
    assessRecord: '访问详情',
    fast: '快速入口',
    institute: '机构管理',
    company: '公司管理',
    organizations: '组织管理',
    employee: '员工管理',
    account: '账号管理',
    role: '角色授权',
    login: '登录日志',
    authorization: '数据授权',
    process: '流程监控',
    note: '短信日志',
    mail: '邮箱日志',
    authCode: '验证码日志',
  },
  workBoard: {
    work: '工作',
    prompt:
      '本页面为展板效果展示，仅展示前端页面的查询条件、模块区域、指标、图形等样式，未连接后台数据接口。您可以参考本页面，实现您的工作展板页面。',
    customDate: '自定义',
    holidayStatis: '人员请假、加班、出差统计',
    fastEntry: '快捷入口',
    borrow: '借款分布',
    entertain: '招待费分布',
  },
  homeBoard: {
    home: '首页',
  },
  bomeBoard: {
    init: '初始化',
  },
};
